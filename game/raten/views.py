from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from random import random, randint 

# Create your views here.
 
secretnumber = None
guessnumbers = []

def play(request):
    global secretnumber
    global guessnumbers
    context = {}
    if secretnumber == None:
        secretnumber = random.randint(1, 100)
        guessnumbers = []
    if "raten" in request.POST:
        guessnumbers = request.POST.get("number")
        if guessnumbers == secretnumber:
            context[guessnumbers] = "richtig"
        elif guessnumbers > secretnumber:
            context[guessnumbers] = "zu groß"
        elif guessnumbers < secretnumber:
            context[guessnumbers] = "zu klein" 
    return render(request, 'raten/index.html', conetext)


